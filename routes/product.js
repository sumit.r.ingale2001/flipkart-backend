import { Router } from 'express';
import { addProducts, addToCart, getProduct, getProducts } from '../controllers/product.js';

const router = Router();


router.post("/", addProducts);
router.get("/", getProducts);
router.get("/:id", getProduct)
router.post("/cart", addToCart)


export default router;