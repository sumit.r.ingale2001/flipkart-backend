import { Router } from 'express';
import { changePassword, deleteCartItem, getCart, loginUser, removeAllFromCart, signUp, verfiyUser, verify } from '../controllers/user.js';


const router = Router();


router.post("/signup", signUp);
router.post('/login', loginUser);
router.get("/verify", verify, verfiyUser)
router.put("/changePassword", changePassword)
router.get("/cart/:userID", getCart)
router.put("/", deleteCartItem)
router.put("/removeAll", removeAllFromCart)


export default router;