import express from 'express';
import cors from 'cors';
import Connection from './database/db.js';
import productRoutes from './routes/product.js'
// import DefaultData from './default.js';
import userRoutes from './routes/user.js';
import cookieParser from 'cookie-parser';
import paymentRoutes from './routes/payment.js'

const PORT = 8000;
const app = express();
const corsConfig = {
    origin: ["https://flipkartclone-aa835.web.app"],
    credentials: true,
    methods: ['GET', 'PUT', 'POST', 'DELETE']
}

app.use(express.json());
app.use(cookieParser())
app.use(cors(corsConfig));


app.use("/products", productRoutes);
app.use("/user", userRoutes);
app.use("/payment", paymentRoutes)



Connection();
app.listen(PORT, () => console.log(`server started on port ${PORT}`));


// add data once
// DefaultData();
